class Barchart extends Frame {
  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 40;
  boolean drawLabels = true;
  float spacer = 5;
  int xTextx, xTexty;
  int yTextx, yTexty;
  
   Barchart( int idx0, int idx1 ){
     this.idx0 = idx0;
     this.idx1 = idx1;
     table.sort(idx0);
     getMinMax();
   }
   
   /*-----------------------------------------------------**/
   void draw(){
     float temp = 800-2*border;
     float barWidth = (temp - temp/(table.getRowCount())) / table.getRowCount();
     float prevX = u0 + border;
     for( int i = 0; i < table.getRowCount(); i++ ){
        TableRow r = table.getRow(i);
        String pointInfo = "";
        float x = prevX;
        float y = map( r.getFloat(idx1), minY, maxY, v0+h-border-spacer, v0+border+spacer );
        
        stroke( 0 );
        fill(255,0,0);         //<>//
        rect(x,y,barWidth,v0+h-border-y);
        fill(0);
        if(mouseInside(x,y,barWidth,v0+h-border-2-y)){
          for(int j = 0; j < table.getColumnCount(); j++){
            pointInfo += "  " + table.getColumnTitle(j) + " " + r.getFloat(j);
          }
          text("Additional Bar Information:" + pointInfo, u0+border+spacer, v0+border+spacer-10);
        }
        prevX = barWidth + prevX;
     }
     
     stroke(0);
     noFill();
     rect( u0+border,v0+border, w-2*border, h-2*border);
     
     if( drawLabels ){
       fill(0);
       xTextx = u0+width/2;
       xTexty = v0+height-10;
       //rect(xTextx - 25, xTexty - 25, 100, 25);
       text( table.getColumnTitle(idx0), xTextx, xTexty );
       pushMatrix();
       yTextx = u0+10;
       yTexty = v0+height/2;
       //rect(yTextx, yTexty, 25, 100);
       translate( yTextx, yTexty);
       rotate( PI/2 );
       text( table.getColumnTitle(idx1), 0, 0 );
       popMatrix();
     }
   }
   
   /*-----------------------------------------------------**/
   void mousePressed(){
     // Mouse inside X label
     if(mouseInside(xTextx - 25, xTexty - 25, 100, 25)){
       if(this.idx0 == table.getColumnCount()-1){
         this.idx0 = 0;
       } else {
         this.idx0 += 1;
       }
       table.sort(idx0);
       getMinMax();
     }
     
     // mouse inside Y label
     if(mouseInside(yTextx, yTexty, 25, 100)){
       if(this.idx1 == table.getColumnCount()-1){
         this.idx1 = 0;
       } else {
         this.idx1 += 1;
       }
       getMinMax();
     }
   }
   
   /*-----------------------------------------------------**/
   // I mean.. yeah it does what it says. You figure it out
   boolean mouseInside(float x, float y, float w, float h){
     boolean a = x < mouseX && (x+w) > mouseX;
     boolean b = (y + h) > mouseY;
     boolean c = y < mouseY;
     return a && b && c;
   }
   
   /*-----------------------------------------------------**/
   // Need this on more than one occasion, so
   // it gets its own function
   void getMinMax(){
     minX = min(table.getFloatColumn(idx0));
     maxX = max(table.getFloatColumn(idx0));
     minY = min(table.getFloatColumn(idx1));
     maxY = max(table.getFloatColumn(idx1));
   }
}