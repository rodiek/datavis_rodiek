
class Scatterplot extends Frame {
  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 40;
  boolean drawLabels = true;
  float spacer = 5;
  int xTextx, xTexty;
  int yTextx, yTexty;
  
   Scatterplot( int idx0, int idx1 ){
     this.idx0 = idx0;
     this.idx1 = idx1;
     getMinMax();
   }
   
   void draw(){
     
     for( int i = 0; i < table.getRowCount(); i++ ){
        TableRow r = table.getRow(i);
        String pointInfo = "";
        
        float x = map( r.getFloat(idx0), minX, maxX, u0+border+spacer, u0+w-border-spacer );
        float y = map( r.getFloat(idx1), minY, maxY, v0+h-border-spacer, v0+border+spacer );
        
        stroke( 0 );
        fill(255,0,0);
        ellipse( x,y,3,3 );
        fill(0);
        if(mouseInside(x-4,y-4,4,4)){
          for(int j = 0; j < table.getColumnCount(); j++){
            pointInfo += "  " + table.getColumnTitle(j) + " " + r.getFloat(j);
          }
          //text("Additional Point Information:" + pointInfo, u0+border+spacer, v0+border+spacer-10);
        }
     }
     
     stroke(0);
     noFill();
     rect( u0+border,v0+border, w-2*border, h-2*border);
     
     if( drawLabels ){
       fill(0);
       xTextx = u0+width/2;
       xTexty = v0+height-10;
       //rect(xTextx - 25, xTexty - 25, 100, 25);
       text( table.getColumnTitle(idx0), xTextx, xTexty );
       pushMatrix();
       yTextx = u0+10;
       yTexty = v0+height/2;
       //rect(yTextx, yTexty, 25, 100);
       translate( yTextx, yTexty);
       rotate( PI/2 );
       text( table.getColumnTitle(idx1), 0, 0 );
       popMatrix();
     }
   }
   
   void mousePressed(){
     // Mouse inside X label
     if(mouseInside(xTextx - 25, xTexty - 25, 100, 25)){
       if(this.idx0 == table.getColumnCount()-1){
         this.idx0 = 0;
       } else {
         this.idx0 += 1;
       }
     }
     
     // mouse inside Y label
     if(mouseInside(yTextx, yTexty, 25, 100)){
       if(this.idx1 == table.getColumnCount()-1){
         this.idx1 = 0;
       } else {
         this.idx1 += 1;
       }
     }
     getMinMax();
   }
   
   // I mean.. yeah it does what it says. You figure it out
   boolean mouseInside(float x, float y, int w, int h){
     boolean a = x < mouseX && (x+w) > mouseX;
     boolean b = (y + h) > mouseY;
     boolean c = y < mouseY;
     return a && b && c;
   }
   
   // Need this on more than one occasion, so
   // it gets its own function
   void getMinMax(){
     minX = min(table.getFloatColumn(idx0));
     maxX = max(table.getFloatColumn(idx0));
     minY = min(table.getFloatColumn(idx1));
     maxY = max(table.getFloatColumn(idx1));
   }
}