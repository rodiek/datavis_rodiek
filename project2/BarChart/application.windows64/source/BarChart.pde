/* Andrew Rodiek      *
 * Project 2          *
 * DataVisualization  *
 * Spring 2018        */
 
// Input processing based on examples on Processing website.
// https://www.processing.org/reference/selectInput_.html
//
// Vertical text from processing forums
// https://forum.processing.org/one/topic/vertical-text.html
public input[] inputArr;
public String[]  labels;
float barWidth = 25;
float spacing = 10;
float yScale = 0;

void setup(){
  size(600,600);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    exit();
  } else {
    processInput(selection.getAbsolutePath());
  }
}

void processInput(String path){
  String[] lines = loadStrings(path);
  inputArr = new input[lines.length - 1];
  
  for(int i = 0; i < lines.length; i++) {
    String[] temp = lines[i].split(",");
    if(i == 0)
      labels = temp;
    else
      inputArr[i - 1] = new input(Float.parseFloat(temp[0]), Float.parseFloat(temp[1]), Float.parseFloat(temp[2]), temp[3]);
  }
  barWidth = (500 - (inputArr.length + 1) * spacing) / inputArr.length;
}

void draw(){
 int prevX = 0;
 int baseY = 549;

 background(255);
 stroke(0);
 line(50, 550, 550, 550);
 line(50, 50, 50, 550);
 
 // Bar Graph
 if(inputArr != null){
   // Y axis labels
   int totalH = (int)(500 / yScale);
   int scaling = totalH / 5;
   int labelH = 450;
   for(int i = 0; i <= 15; i++){
     text(scaling * (i + 1), 25, labelH);
     stroke(211,211,211);
     line(51, labelH, 551, labelH);
     labelH -= 100;
   }
   
   float maxHeight = 0;
   for(int i = 0; i < inputArr.length; i++){
     if(inputArr[i].third > maxHeight)
       maxHeight = inputArr[i].third;
     yScale = 500/maxHeight;
   }
   
   for(int i = 0; i < inputArr.length; i++) {
     inputArr[i].draw(prevX + barWidth + spacing, baseY, barWidth, yScale);
     prevX += barWidth + spacing;
   }
 }
 
 // Label Graph
 if(labels != null){
  fill(0);
  // Vertical
  pushMatrix();
  translate(20,300);
  rotate(-HALF_PI);
  text(labels[2],0,0);
  popMatrix(); 
  // Horizontal
  text(labels[0], 300, 585);
 }
}