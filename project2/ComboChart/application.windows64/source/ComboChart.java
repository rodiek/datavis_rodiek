import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ComboChart extends PApplet {

/* Andrew Rodiek      *
 * Project 2          *
 * DataVisualization  *
 * Spring 2018        */
 
// Input processing based on examples on Processing website.
// https://www.processing.org/reference/selectInput_.html
//
// Vertical text from processing forums
// https://forum.processing.org/one/topic/vertical-text.html
public input[] inputArr;
public String[]  labels;
float barWidth = 25;
float spacing = 10;
float yScale = 0;
float lineSpacing = 10;
float lineScaleY = 0;

public void setup(){
  
  selectInput("Select a file to process:", "fileSelected");
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    exit();
  } else {
    processInput(selection.getAbsolutePath());
  }
}

public void processInput(String path){
  String[] lines = loadStrings(path);
  inputArr = new input[lines.length - 1];
  
  for(int i = 0; i < lines.length; i++) {
    String[] temp = lines[i].split(",");
    if(i == 0)
      labels = temp;
    else
      inputArr[i - 1] = new input(Float.parseFloat(temp[0]), Float.parseFloat(temp[1]), Float.parseFloat(temp[2]), temp[3]);
  }
  barWidth = (500 - (inputArr.length + 1) * spacing) / inputArr.length;
  lineSpacing = 500/(inputArr.length + 1);
}

public void draw(){
 int prevX = 0;
 int baseY = 549;
 float linePrevX = -1;
 float linePrevY = -1;

 background(255);
 stroke(0);
 line(50, 550, 550, 550);
 line(50, 50, 50, 550);
 
 // Bar Graph
 if(inputArr != null){
   // Y axis labels
   int totalH = (int)(500 / yScale);
   int scaling = totalH / 5;
   int labelH = 450;
   for(int i = 0; i <= 15; i++){
     text(scaling * (i + 1), 25, labelH);
     stroke(211,211,211);
     line(51, labelH, 551, labelH);
     labelH -= 100;
   }
   
   float maxHeight = 0;
   for(int i = 0; i < inputArr.length; i++){
     if(inputArr[i].third > maxHeight)
       maxHeight = inputArr[i].third;
   }
   yScale = 500/maxHeight;
   
   // Draw Bars
   for(int i = 0; i < inputArr.length; i++) {
     inputArr[i].draw(prevX + barWidth + spacing, baseY, barWidth, yScale);
     prevX += barWidth + spacing;
   }
   
   // Finding height for line scaling
   float lineMaxHeight = 0;
   for(int i = 0; i < inputArr.length; i++){
     if(inputArr[i].second > lineMaxHeight)
       lineMaxHeight = inputArr[i].second;
   }
   lineScaleY = 500/lineMaxHeight;
   
   int lineTotalH = (int)(500 / lineScaleY);
   float lineScaling = (float)lineTotalH / 5;
   //draw points for line graph
   for(int i = 0; i < inputArr.length; i++) {
     inputArr[i].draw(linePrevX, linePrevY, baseY, lineSpacing, lineScaleY);
     linePrevY = inputArr[i].yVal;
     linePrevX = inputArr[i].xVal;
   }
 }
 
 // Label Graph
 if(labels != null){
  fill(0);
  // Vertical
  pushMatrix();
  translate(20,300);
  rotate(-HALF_PI);
  text(labels[2],0,0);
  popMatrix(); 
  // Horizontal
  text(labels[0], 300, 585);
 }
}
class input {
 public float first = 0;
 public float second = 0.0f;
 public float third = 0;
 public String fourth = "";
 public float xVal = 0;
 public float yVal = 0;
 
 input(float iFirst, float iSecond, float iThird, String iFourth){
   first = iFirst;
   second = iSecond;
   third = iThird;
   fourth = iFourth;
 }
 
 public void draw(float refX, float base, float barWidth, float yScale) {
   if(fourth.indexOf("REP") > -1){
     stroke(255, 0, 0);
     fill(255,0,0);
   }
   if(fourth.indexOf("DEM") > -1){
     stroke(0,0,255);
     fill(0,0,255);
   }
   int newHeight = (int)(yScale * third);
   rect(refX, base - newHeight, barWidth, newHeight);
   fill(0);
   // X label
   text((int)first, refX + barWidth / 4, base + 15);
 }
 
 // Line Chart
 public void draw(float prevX, float prevY, float base, float spacing, float yScale){
   fill(0);
   stroke(0);
   strokeWeight(10);
   yVal = base - (yScale * second);
   if(prevX != -1 && prevY != -1){
     xVal = prevX + spacing;
     point(xVal, yVal);
   strokeWeight(1);
     line(prevX, prevY, xVal, yVal);
   } else {
     xVal = 50 + spacing; 
     point(xVal, yVal);
   }
 }
}
  public void settings() {  size(600,600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "ComboChart" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
