class input {
 public float first = 0;
 public float second = 0.0;
 public float third = 0;
 public String fourth = "";
 public float xVal = 0;
 public float yVal = 0;
 
 input(float iFirst, float iSecond, float iThird, String iFourth){
   first = iFirst;
   second = iSecond;
   third = iThird;
   fourth = iFourth;
 }
 
 void draw(float refX, float base, float barWidth, float yScale) {
   if(fourth.indexOf("REP") > -1){
     stroke(255, 0, 0);
     fill(255,0,0);
   }
   if(fourth.indexOf("DEM") > -1){
     stroke(0,0,255);
     fill(0,0,255);
   }
   int newHeight = (int)(yScale * third); //<>//
   rect(refX, base - newHeight, barWidth, newHeight);
   fill(0);
   // X label
   text((int)first, refX + barWidth / 4, base + 15);
 }
 
 // Line Chart
 void draw(float prevX, float prevY, float base, float spacing, float yScale){
   fill(0);
   stroke(0);
   strokeWeight(10);
   yVal = base - (yScale * second);
   if(prevX != -1 && prevY != -1){
     xVal = prevX + spacing;
     point(xVal, yVal);
   strokeWeight(1);
     line(prevX, prevY, xVal, yVal);
   } else {
     xVal = 50 + spacing; 
     point(xVal, yVal);
   }
 }
}