Test files are Auto.csv, and testInput.txt. They are located in the Project 3 root directory.

I had some issues while selecting files and getting a null exception.
To fix this, make sure to hit stop in between testing, and also make
sure to wait a second or two before selecting a file when the program launches. 