/* Andrew Rodiek      *
 * Project 3          *
 * DataVisualization  *
 * Spring 2018        */
 
// Input processing based on examples on Processing website.
// https://www.processing.org/reference/selectInput_.html
//
// Vertical text from processing forums
// https://forum.processing.org/one/topic/vertical-text.html

public input[] inputArr;
public String[]  labels;

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    exit();
  } else {
    processInput(selection.getAbsolutePath());
  }
}

void processInput(String path){
  String[] lines = loadStrings(path);
  inputArr = new input[lines.length - 1];
  float holder;
  
  for(int i = 0; i < lines.length; i++) {
    String[] temp = lines[i].split(",");
    if(i == 0){
      labels = temp;
    } else {
      if(temp[3].contains("Grade 1"))
        holder = 1;
      else if(temp[3].contains("Grade 2"))
        holder = 2;
      else if(temp[3].contains("Grade 3"))
        holder = 3;
      else if(temp[3].contains("Grade 4"))
        holder = 4;
      else
        holder = Float.parseFloat(temp[3]);
      inputArr[i - 1] = new input(Float.parseFloat(temp[0]), Float.parseFloat(temp[1]), Float.parseFloat(temp[2]), holder);
    }
  }
}

//=============================================================================================
void draw(){
  //Max and Mins of data
  float[] maxVals = {0,0,0,0};
  float[] minVals = new float[4];
  
  // Set background colors
  background(255);
  stroke(0);
  // Graph boundaries
  noFill();
  rect(50, 50, 700, 500);
  rect(225, 50, 350, 500);
  line(400, 50, 400, 550);
  rect(50, 175, 700, 250);
  line(50, 300, 750, 300);
  
  if(inputArr != null){
    //Initialize minVals
    minVals[0] = inputArr[0].first;
    minVals[1] = inputArr[0].second;
    minVals[2] = inputArr[0].third;
    minVals[3] = inputArr[0].fourth;
    
    // Find max and mins
    for(int i = 0; i < inputArr.length; i++){
      maxVals[0] = max(inputArr[i].first, maxVals[0]);
      maxVals[1] = max(inputArr[i].second, maxVals[1]);
      maxVals[2] = max(inputArr[i].third, maxVals[2]);
      maxVals[3] = max(inputArr[i].fourth, maxVals[3]);     
      
      minVals[0] = min(inputArr[i].first, minVals[0]);
      minVals[1] = min(inputArr[i].second, minVals[1]);
      minVals[2] = min(inputArr[i].third, minVals[2]);
      minVals[3] = min(inputArr[i].fourth, minVals[3]);
    }
    
    // Boundaries for each square of matrix
    int[] xBounds = {50, 225, 400, 575, 750};
    int[] yBounds = {50, 175, 300, 425, 550};
    
    // Draw encodings
    for(int i = 0; i < inputArr.length; i++) {
      fill(0);
      stroke(0);
      // SATM vs SATV
      float x0 = map(inputArr[i].first, minVals[0], maxVals[0], xBounds[0] + 2, xBounds[1] - 2);
      float y0 = map(inputArr[i].second, minVals[1], maxVals[1], yBounds[2] - 2, yBounds[1] + 2);
      ellipse(x0, y0, 2, 2);
      // SATM vs ACT
      float x1 = map(inputArr[i].first, minVals[0], maxVals[0], xBounds[0] + 2, xBounds[1] - 2);
      float y1 = map(inputArr[i].third, minVals[2], maxVals[2], yBounds[3] - 2, yBounds[2] + 2);
      ellipse(x1, y1, 2, 2);
      // SATM vs GPA
      float x2 = map(inputArr[i].first, minVals[0], maxVals[0], xBounds[0] + 2, xBounds[1] - 2);
      float y2 = map(inputArr[i].fourth, minVals[3], maxVals[3], yBounds[4] - 2, yBounds[3] + 2);
      ellipse(x2, y2, 2, 2);
      
      fill(255,0,0);
      stroke(255,0,0);
      // SATV vs SATM
      float x3 = map(inputArr[i].second, minVals[1], maxVals[1], xBounds[1] + 2, xBounds[2] - 2);
      float y3 = map(inputArr[i].first, minVals[0], maxVals[0], yBounds[1] - 2, yBounds[0] + 2);
      ellipse(x3, y3, 2, 2);
      // SATV vs ACT
      float x4 = map(inputArr[i].second, minVals[1], maxVals[1], xBounds[1] + 2, xBounds[2] - 2);
      float y4 = map(inputArr[i].third, minVals[2], maxVals[2], yBounds[3] - 2, yBounds[2] + 2);
      ellipse(x4, y4, 2, 2);
      // SATV vs GPA
      float x5 = map(inputArr[i].second, minVals[1], maxVals[1], xBounds[1] + 2, xBounds[2] - 2);
      float y5 = map(inputArr[i].fourth, minVals[3], maxVals[3], yBounds[4] - 2, yBounds[3] + 2);
      ellipse(x5, y5, 2, 2);
      
      fill(0,255,0);
      stroke(0,255,0);
      // ACT vs SATM
      float x6 = map(inputArr[i].third, minVals[2], maxVals[2], xBounds[2] + 2, xBounds[3] - 2);
      float y6 = map(inputArr[i].first, minVals[0], maxVals[0], yBounds[1] - 2, yBounds[0] + 2);
      ellipse(x6, y6, 2, 2);
      // ACT vs SATV
      float x7 = map(inputArr[i].third, minVals[2], maxVals[2], xBounds[2] + 2, xBounds[3] - 2);
      float y7 = map(inputArr[i].second, minVals[1], maxVals[1], yBounds[2] - 2, yBounds[1] + 2);
      ellipse(x7, y7, 2, 2);
      // ACT vs GPA
      float x8 = map(inputArr[i].third, minVals[2], maxVals[2], xBounds[2] + 2, xBounds[3] - 2);
      float y8 = map(inputArr[i].fourth, minVals[3], maxVals[3], yBounds[4] - 2, yBounds[3] + 2);
      ellipse(x8, y8, 2, 2);
      
      fill(0,0,255);
      stroke(0,0,255);
      // GPA vs SATM
      float x9 = map(inputArr[i].fourth, minVals[3], maxVals[3], xBounds[3] + 2, xBounds[4] - 2);
      float y9 = map(inputArr[i].first, minVals[0], maxVals[0], yBounds[1] - 2, yBounds[0] + 2);
      ellipse(x9, y9, 2, 2);
      // GPA vs SATV
      float x10 = map(inputArr[i].fourth, minVals[3], maxVals[3], xBounds[3] + 2, xBounds[4] - 2);
      float y10 = map(inputArr[i].second, minVals[1], maxVals[1], yBounds[2] - 2, yBounds[1] + 2);
      ellipse(x10, y10, 2, 2);
      // GPA vs ACT
      float x11 = map(inputArr[i].fourth, minVals[3], maxVals[3], xBounds[3] + 2, xBounds[4] - 2);
      float y11 = map(inputArr[i].third, minVals[2], maxVals[2], yBounds[3] - 2, yBounds[2] + 2);
      ellipse(x11, y11, 2, 2);
    }
 }
 // Label Graph
 if(labels != null){
   fill(0);
   int xInt = 135;
   int yInt = 110;
   for(int i = 0; i < labels.length; i++){
     // Horizontal
     text(labels[i], xInt, 575);
     xInt += 175;
     // Vertical
     pushMatrix();
     translate(30, yInt);
     rotate(-HALF_PI);
     text(labels[i],0,0);
     popMatrix(); 
     yInt += 125;
   }
 }
}