/* Andrew Rodiek      *
 * Project 3          *
 * DataVisualization  *
 * Spring 2018        */
 
// Input processing based on examples on Processing website.
// https://www.processing.org/reference/selectInput_.html
//
// Vertical text from processing forums
// https://forum.processing.org/one/topic/vertical-text.html

public input[] inputArr;
public String[]  labels;
float barWidth = 25;
float spacing = 10;
float yScale = 0;

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    exit();
  } else {
    processInput(selection.getAbsolutePath());
  }
}

void processInput(String path){
  String[] lines = loadStrings(path);
  inputArr = new input[lines.length - 1];
  
  for(int i = 0; i < lines.length; i++) {
    String[] temp = lines[i].split(",");
    if(i == 0)
      labels = temp;
    else
      inputArr[i - 1] = new input(Float.parseFloat(temp[0]), Float.parseFloat(temp[1]), Float.parseFloat(temp[2]), Float.parseFloat(temp[3]));
  }
  barWidth = (500 - (inputArr.length + 1) * spacing) / inputArr.length;
}

//=============================================================================================
void draw(){
  //Max and Mins of data
  float[] maxVals = {0,0,0,0};
  float[] minVals = new float[4];
  
  // Window max and min for encodings
  int xMax = 748;
  int xMin = 52;
  int yMax = 548;
  int yMin = 52;
  
  // Set background colors
  background(255);
  stroke(0);
  // Graph boundaries
  line(50, 550, 750, 550);
  line(50, 50, 50, 550);
  
  if(inputArr != null){
    //Initialize minVals
    minVals[0] = inputArr[0].first;
    minVals[1] = inputArr[0].second;
    minVals[2] = inputArr[0].third;
    minVals[3] = inputArr[0].fourth;
    
    // Find max and mins
    for(int i = 0; i < inputArr.length; i++){
      //maxVals[0] = max(inputArr[i].first, maxVals[0]);
      //maxVals[1] = max(inputArr[i].second, maxVals[1]);
      maxVals[2] = max(inputArr[i].third, maxVals[2]);
      maxVals[3] = max(inputArr[i].fourth, maxVals[3]);     
      
      //minVals[0] = min(inputArr[i].first, minVals[0]);
      //minVals[1] = min(inputArr[i].second, minVals[1]);
      minVals[2] = min(inputArr[i].third, minVals[2]);
      minVals[3] = min(inputArr[i].fourth, minVals[3]);
    }
    
    // Markings (xMin, xMax, yMin, yMax)
    drawLabels(minVals[2], maxVals[2], minVals[3], maxVals[3]);
    
    
    // Draw encodings
    for(int i = 0; i < inputArr.length; i++) {
      float x = map(inputArr[i].third, minVals[2], maxVals[2], xMin, xMax);
      float y = map(inputArr[i].fourth, minVals[3], maxVals[3], yMax, yMin);
      ellipse(x, y, 2, 2);
    }
    
 }
 // Label Graph
 if(labels != null){
   fill(0);
   // Vertical
   pushMatrix();
   translate(20,300);
   rotate(-HALF_PI);
   text(labels[3],0,0);
   popMatrix(); 
   // Horizontal
   text(labels[2], 400, 585);
 }
}

void drawLabels(float xmin, float xmax, float ymin, float ymax){
  // X Low
  line(50, 550, 50, 552);
  text(Integer.toString((int)xmin), 40, 570);
  // X High
  line(750, 550, 750, 552);
  text(Integer.toString((int)xmax), 740, 570);
  // X Incremental
  float increment = (xmax - xmin) / 3;
  int tmpInc = (int)(xmin + increment);
  int intervals = 700 / 3;
  int tmpInt = 50 + intervals;
  for(int i = 0; i < 2; i++){
    text(Integer.toString(tmpInc), tmpInt, 570);
    line(tmpInt + 10, 550, tmpInt + 10, 552);
    tmpInc += increment;
    tmpInt += intervals;
  }
  
  // Y Vals =====================================
  // Y Low
  line(50, 550, 48, 550);
  text(Integer.toString((int)ymin), 25, 555);
  // Y High
  line(50, 50, 48, 50);
  text(Integer.toString((int)ymax), 25, 55);
  // X Incremental
  float yIncrement = (ymax - ymin) / 3;
  int yTmpInc = (int)(ymin + yIncrement);
  
  int yIntervals = 500 / 3;
  int yTmpInt = 500 - yIntervals;
  for(int i = 0; i < 2; i++){
    text(Integer.toString(yTmpInc), 25, yTmpInt);
    line(50, yTmpInt, 48, yTmpInt);
    yTmpInc += yIncrement;
    yTmpInt -= yIntervals;
  }
}