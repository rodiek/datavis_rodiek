// most modification should occur in this file


class ForceDirectedLayout extends Frame {


  float RESTING_LENGTH = 50.0f;   // update this value
  float SPRING_SCALE   = 0.015f; // update this value
  float REPULSE_SCALE  = 400.0f;  // update this value

  float TIME_STEP      = 0.5f;    // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;

  // Color code for groups
  int[] colorGroup = 
    { #5DA5DA, // blue
    #FAA43A, // orange
    #60BD68, // green
    #F17CB0, // pink
    #B2912F, // brown
    #B276B2, // purple
    #DECF3F, // yellow
    #F15854, // red
    #FFFFFF, // white
    #000080, // dark blue
    #00FF00 }; // green


  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {

    float dx = v0.getPosition().x - v1.getPosition().x;
    float dy = v0.getPosition().y - v1.getPosition().y;
    float d = sqrt(pow(dx, 2.0f) + pow(dy, 2.0f));

    float fx = (REPULSE_SCALE * v0.getMass() * v1.getMass()) / pow(d, 2.0f) * (dx / d);
    float fy = (REPULSE_SCALE * v0.getMass() * v1.getMass()) / pow(d, 2.0f) * (dy / d);

    v0.addForce(fx, fy);
  }

  void applySpringForce( GraphEdge edge ) {

    float dx = edge.v1.getPosition().x - edge.v0.getPosition().x;
    float dy = edge.v1.getPosition().y - edge.v0.getPosition().y;
    float d = sqrt(pow(dx, 2.0f) + pow(dy, 2.0f));

    float fx = -SPRING_SCALE * (d - RESTING_LENGTH) * (dx / d);
    float fy = -SPRING_SCALE * (d - RESTING_LENGTH) * (dy / d);

    edge.v0.addForce(-fx, -fy);
    edge.v1.addForce(fx, fy);
  }

  void draw() {
    update(); // don't modify this line

    if (selected != null) 
      selected.setPosition(mouseX, mouseY);
    noFill();
    stroke(255);
    for (GraphEdge e : edges) {
      line(e.v0.getPosition().x, e.v0.getPosition().y, e.v1.getPosition().x, e.v1.getPosition().y);
    }

    for (GraphVertex v : verts) {
      stroke(colorGroup[v.group]);
      fill(colorGroup[v.group]);
      ellipse(v.getPosition().x, v.getPosition().y, v.diam, v.diam);

      // pop up text box for vertex info
      if (mouseInside(v.getPosition(), v.diam)) {
        fill(255);
        stroke(255);
        rect(mouseX, mouseY - 12, textWidth(v.id) + 2, 13);
        fill(0);
        stroke(0);
        textAlign(LEFT, TOP);
        text(v.id, mouseX + 3, mouseY - 12);
      }
      // mouse over info
      String info = "Mouse over point to see Characer Name";
      stroke(255);
      fill(255);
      textAlign(LEFT, BOTTOM);
      text(info, u0, v0+h);
    }

    // Legend
    int diff = 15;
    int prev = 15;
    textAlign(LEFT, TOP);
    text("Group Color Legend", u0, v0);
    for (int g = 0; g < colorGroup.length; g++) {
      fill(colorGroup[g]);
      stroke(colorGroup[g]);
      rect(u0, v0 + prev, 15, 15);
      fill(255);
      stroke(255);
      text("Group " + g, u0 + 17, v0 + prev);
      prev += diff;
    }
  }


  void mousePressed() { 
    // search through points and grap the one the mouse is inside of to move
    for (GraphVertex v : verts) {
      if (mouseInside(v.getPosition(), v.diam)) {
        selected = v;
        break;
      }
    }
  }

  void mouseReleased() {    
    // drop the point
    selected = null;
  }

  // mouseInside specifically for points
  boolean mouseInside(PVector coord, float diam) {
    return ((coord.x - diam - clickBuffer) < mouseX) && (coord.x + diam + clickBuffer)>mouseX 
      && (coord.y - diam - clickBuffer)< mouseY && (coord.y + diam + clickBuffer)>mouseY;
  }

  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}