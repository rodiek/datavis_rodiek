import java.util.Random;

Frame myFrame = null;

void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();
    HashMap<String, GraphVertex> forEdges = new HashMap<String, GraphVertex>();

    // TODO: PUT CODE IN TO LOAD THE GRAPH  
    JSONObject json = loadJSONObject(selection);
    JSONArray nodes = json.getJSONArray("nodes");
    JSONArray links = json.getJSONArray("links");
    Random rand = new Random();
    
    // Fill Vert array from json
    for(int i = 0; i < nodes.size(); i++) {
      JSONObject j = nodes.getJSONObject(i);
      verts.add(new GraphVertex(j.getString("id"), j.getInt("group"), rand.nextInt(800), rand.nextInt(800))); 
      forEdges.put(j.getString("id"), verts.get(verts.size()-1));
    }
    
    // Fill edge array from json
    for(int i = 0; i < links.size(); i++) {
      JSONObject j = links.getJSONObject(i);
      edges.add(new GraphEdge(forEdges.get(j.getString("source")), forEdges.get(j.getString("target")), j.getInt("value")));
    }
    
    myFrame = new ForceDirectedLayout( verts, edges ); //<>//
  }
}

void draw() {
  background( 0 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 3;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}