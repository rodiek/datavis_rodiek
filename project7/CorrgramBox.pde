class CorrgramBox extends Frame {
  float pearson;
  float spacer;
  // Constructor 
  CorrgramBox(float val) {
    // set color
    pearson = val;
  }

  void draw() {
    strokeWeight(0);
    stroke(0);
    rect(u0, v0, w, h);

    strokeWeight(2);
    if (pearson < 0) {
      stroke(255, 0, 0);
      line(u0, v0, u0+w, v0+h);
    } else if (pearson == 0) {
      stroke(0, 0, 0);
      line(u0, v0-(h/2), u0+w, v0-(h/2));
    } else {
      stroke(0, 0, 255);
      line(u0, v0+h, u0+w, v0);
    }
  }
  void mousePressed() {
  }
}