ArrayList<Frame> myFrames = new ArrayList<Frame>();
Frame bigFrame = null;
Table table;

void setup() {
  size(1200, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );

    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for (int i = 0; i < table.getColumnCount(); i++) {
      if ( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ) {
        println( i + " - type float" );
        useColumns.add(i);
      } else {
        println( i + " - type string" );
      }
    }

    myFrames.add(new Histogram(0, true));
    myFrames.add(new Histogram(1, true));
    myFrames.add(new Histogram(2, true));
    myFrames.add(new Histogram(3, true));

    myFrames.get(0).setPosition(0, 0, 300, 400);
    myFrames.get(1).setPosition(300, 0, 300, 400);
    myFrames.get(2).setPosition(0, 400, 300, 400);
    myFrames.get(3).setPosition(300, 400, 300, 400);

    myFrames.add( new Corrgram());
    myFrames.get(4).setPosition(600, 0, 600, 400);
    
    myFrames.add(new Spearman());
    myFrames.get(5).setPosition(600,400,600,400);
  }
}


void draw() {
  background( 255 );
  if ( table == null ) 
    return;

  for (int i = 0; i < 6; i ++) {
    myFrames.get(i).draw();
  }
}


void mousePressed() {
    for(Frame f: myFrames){
     f.mousePressed(); 
    }
}

abstract class Frame {
  int u0, v0, w, h;
  int clickBuffer = 2;
  boolean drawLabels = true;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  void mousePressed() {
  }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
}