import java.util.Collections; //<>//
class Histogram extends Frame {
  double min, max;
  int idx0;
  float spacer = 15;
  int xTextx, xTexty;
  int bins = 5;
  int maxBins = 20;
  int minBins = 3;

  ArrayList<Integer> binVals = new ArrayList<Integer>(bins);
  ArrayList<Float> binBorder = new ArrayList<Float>(bins);

  //Constructor
  Histogram( int idx0, boolean labels ) {
    this.idx0 = idx0;
    this.drawLabels = labels;
    getMinMax();
    initArrays();
  }

  void draw() {

    //set size of each histogram column
    for ( int i = 0; i < table.getRowCount(); i++ ) {
      TableRow r = table.getRow(i);
      // find the correct bin
      for (int j = 0; j < binBorder.size(); j++) {
        // Find correct position
        float temp = r.getFloat(idx0);
        float temp1 = binBorder.get(j);
        if (temp < temp1) {
          binVals.set(j, binVals.get(j) + 1);
          break;
        }
      }
    }

    /* Actually draw the histogram */
    // guidelines 
    strokeWeight(0);
    line(u0 + spacer, v0 + h - spacer, u0 + w - spacer, v0 + h - spacer);
    line(u0 + spacer, v0 + spacer, u0 + spacer, v0 + h - spacer);

    // Bar values
    noFill();
    float barWidth = (w - 2 * spacer) / binVals.size();
    float barX = u0 + spacer;
    for (int k : binVals) {
      float barY = 0;
      float barHeight = 0;
      if (k != 0) {
        barY = map(k, Collections.min(binVals), Collections.max(binVals), v0+h-spacer, v0+spacer );
        barHeight = v0+h-spacer-barY;
      } else {
        barY = v0 + h - spacer;
      }

      stroke(0);
      strokeWeight(1);
      fill(0, 255, 0);
      rect(barX, barY, barWidth, barHeight);
      fill(0);
      noFill();
      if (drawLabels) {
        textAlign(LEFT, TOP);
        text("<" + Integer.toString(Math.round(binBorder.get(binVals.indexOf(k)))), barX+2, v0 + h - spacer);
      }      
      barX += barWidth;
    }

    // graph labels
    if (drawLabels) {
      // Graph Title
      textAlign(RIGHT, BOTTOM);
      text(table.getColumnTitle(idx0) + " Histogram", u0+w/2, v0+spacer);
    }

    /* buttons for changing bin size */
    // add
    rect(u0+w-15, v0+5, 7, 7);
    textAlign(CENTER, TOP);
    text("+", u0+w-10, v0+1);
    // current value
    text(Integer.toString(bins), u0+w-20, v0+1);
    // subtract
    rect(u0+w-35, v0+5, 7, 7);
    textAlign(CENTER, TOP);
    text("-", u0+w-30, v0+1);
    //label
    textAlign(RIGHT, TOP);
    text("Bin Size:", u0+w-45, v0+1);
  }

  // Helpful for mapping
  void getMinMax() {
    min = min(table.getFloatColumn(idx0));
    max = max(table.getFloatColumn(idx0));
  }

  // For bin size changes
  void initArrays() {
    binVals.clear();
    binBorder.clear();     
    //get bin border values
    float val = (float)max / bins;
    for (int i = 0; i < bins; i++) {
      binBorder.add(val * (i+1));
    }
    for (int i = 0; i < bins; i ++) {
      binVals.add(0);
    }
  }

  // I mean.. yeah it does what it says. You figure it out
  boolean mouseInside(float x, float y, float w, float h) {
    boolean a = x < mouseX && (x+w) > mouseX;
    boolean b = (y + h) > mouseY;
    boolean c = y < mouseY;
    return a && b && c;
  }

  // Handle changing bin size
  void mousePressed() {
    /* hard coded values for the buttons...? No, you didn't see that. */
    // add
    if (mouseInside(u0+w-13, v0+3, 10, 10)) {
      if (bins + 1 <= maxBins) {
        bins += 1; 
        initArrays();
      }
    }
    // subtract
    else if (mouseInside(u0+w-35, v0+5, 7, 7)) {
      if (bins - 1 >= minBins) {
        bins -= 1;
        initArrays();
      }
    }
  }
}