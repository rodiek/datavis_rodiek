class Corrgram extends Frame {
  float spacer = 15;
  int colCount;  
  float yTextx, yTexty, xTextx, xTexty;

  // necessary calculations
  ArrayList<Float> mean = new ArrayList<Float>();
  ArrayList<Float> stdv = new ArrayList<Float>();
  ArrayList<Float> covTop = new ArrayList<Float>();
  ArrayList<Float> covBot = new ArrayList<Float>();
  ArrayList<CorrgramBox> Corrs = new ArrayList<CorrgramBox>();
  // Constructor 
  Corrgram() {
    colCount = table.getColumnCount();
  }

  void draw() {

    // Mean Calculation
    for (int i = 0; i < table.getColumnCount(); i++) {
      float mSum = 0.0;
      TableRow r = table.getRow(i);
      for (int j = 0; j < table.getRowCount(); j++) {
        mSum +=  r.getFloat(i);
      }
      mean.add(mSum / table.getRowCount());
    }

    // Initialize arrays
    for (int i = 0; i < colCount; i++) {
      covTop.add(0.0);
      covBot.add(0.0);
    }

    // Standard Deviation
    for (int i = 0; i < table.getColumnCount(); i++) {
      TableRow r = table.getRow(i);
      for (int j = 0; j < table.getRowCount(); j++) {
        covTop.set(i, covTop.get(i) + (r.getFloat(i) - mean.get(i)));
        covBot.set(i, covBot.get(i) + pow(covTop.get(i), 2));
      }
      stdv.add( sqrt(covBot.get(i) / table.getRowCount()));
    }


    // set up corrgram boxes
    for ( int j = 0; j < colCount-1; j++ ) {
      for ( int i = j+1; i < colCount; i++ ) {
        CorrgramBox cb = new CorrgramBox((covTop.get(j) * covTop.get(i)) / (sqrt(covBot.get(j)) * sqrt(covBot.get(i))));
        int su0 = (int)map( i, 1, colCount, u0+spacer, u0+w-spacer );
        int sv0 = (int)map( j, 0, colCount-1, v0+spacer, v0+h-spacer );
        cb.setPosition( su0, sv0, (int)(w-2*spacer)/(colCount-1), (int)(h-2*spacer)/(colCount-1) );
        Corrs.add(cb);
        
        // Draw Mean and Standard Deviation
        if (mouseInside(cb.u0, cb.v0, cb.w, cb.h)) {
          textAlign(LEFT,BOTTOM);
          text(table.getColumnTitle(j) + "  Mean: " + Float.toString(mean.get(j)) + "  Standard Deviation: " + 
            Float.toString(mean.get(j)), u0+spacer, v0+h-20);
          text(table.getColumnTitle(i) + "  Mean: " + Float.toString(mean.get(i)) + "  Standard Deviation: " + 
            Float.toString(mean.get(i)), u0+spacer, v0+h-30);
        }
      }
    }

    // draw corrgram boxes
    for (CorrgramBox b : Corrs) {
      b.draw();
    }

    // draw labels
    if ( drawLabels ) {
      // Chart Name
      stroke(0);
      fill(0);
      textAlign(CENTER, BOTTOM);
      text("Pearson Correlation", u0 + w/2, v0+spacer);
      noFill();

      // Vertical Labels
      float yTextDiff = (h-2*spacer) / colCount;
      yTexty = v0 + yTextDiff;
      for (int i = 0; i < colCount - 1; i++) {
        pushMatrix();
        yTextx = u0+w-spacer;         
        translate( yTextx, yTexty);
        yTexty += yTextDiff;
        rotate( PI/2 );
        textAlign(CENTER, BOTTOM);
        text( table.getColumnTitle(i), 0, 0 );
        popMatrix();
      }

      // Horizontal labels
      float xTextDiff = (w-2*spacer) / colCount;
      xTextx = u0 +xTextDiff;
      xTexty = v0 + h - spacer;
      for (int i = 1; i < colCount; i ++) {
        textAlign(CENTER, TOP);
        text(table.getColumnTitle(i), xTextx, xTexty); 
        xTextx += xTextDiff;
      }
    }
  }

  // I mean.. yeah it does what it says. You figure it out
  boolean mouseInside(float x, float y, float w, float h) {
    boolean a = x < mouseX && (x+w) > mouseX;
    boolean b = (y + h) > mouseY;
    boolean c = y < mouseY;
    return a && b && c;
  }
}