class Spearman extends Frame {
  float pearson;
  float spacer;
  // Constructor 
  Spearman() {
  }

  void draw() {
      stroke(0);
      fill(0);
      textAlign(CENTER, TOP);
      text("Spearman Correlation", u0 + w/2, v0+2*spacer);
      text("Almost had it :(", u0+w/2,v0+h/2);
      noFill();
  }
}