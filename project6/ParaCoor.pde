class ParaCoor extends Frame {
  // Min and max values for point placement
  float minY, maxY;
  
  //Positioning of labels
  float Textx, Texty;
  
  // for iteration through data set
  int colCount;
  ArrayList<Integer> useColumns;
  
  // point width and height
  int pW = 3;
  int pH = 3;
  
  // helpers
  int border = 30;
  float spacer = 5;
  
  // Order of columns in view, & their x vals
  boolean SomethingSelected = false;
  int selectedIndex = -1;
  int[] lineOrder;
  float[] lineX;
  
  /*-----------------------------------------------------**/
  ParaCoor(ArrayList<Integer> useColumns){
    this.useColumns = useColumns;
    colCount = useColumns.size();
    
    // Initializing some helpful variables
    Texty = v0+border-15;
    lineOrder = new int[colCount];
    lineX = new float[colCount];
    // set initial order
    for(int i = 0; i < colCount; i++) {
      lineOrder[i] = i;
    }
  }
  
  /*-----------------------------------------------------**/
  void draw(){
    // Chart
    stroke(0);
    fill(#D3D3D3);
    rect(u0+border,v0+border, w-2*border, h-2*border);
    fill(0);
    
    // Drawing vertical lines
    float diffX = (w - 2*border) / (colCount - 1);
    float tempx = u0+border;
    for(int l = 0; l <= colCount - 1; l++) {
      line(tempx, v0+border, tempx, v0+h-border);
      lineX[l] = tempx;
      tempx += diffX;
    }
    
    // Draw labels
    for(int t = 0; t < colCount; t++) {
      // Color label if it's selected
      if(t == selectedIndex) {
        fill(#32CD32);
        stroke(#32CD32);
      } 
      else {
        fill(0);
        stroke(0);        
      }

      Textx = lineX[t];
      if(drawLabels) {
        textAlign(CENTER);
        text(table.getColumnTitle(lineOrder[t]), Textx, Texty);
      }
    }
    
    // Draw data points and lines
    for(int i = 0; i < table.getRowCount(); i++) {
      TableRow r = table.getRow(i);
      float prevX = -1;
      float prevY = -1;
      
      for(int j = 0; j < colCount; j++) {
        getMinMax(lineOrder[j]);
        float y = map(r.getFloat(lineOrder[j]), minY, maxY, v0+h-border-spacer, v0+border+spacer );
        float x = lineX[j];
        
        // Label max and mins for vertical axis
        if(drawLabels) {
          fill(0);
          textSize(11);
          text((int)maxY, x, v0+border-5);
          text((int)minY, x, v0+h-border+15);
        }
        
        //Finally draw point and line
        fill(#ff0000);
        stroke(#ff0000);
        ellipse(x,y,3,3);
        if(!(prevX == -1) && !(prevY == -1))
          line(x, y, prevX, prevY);
        prevX = x;
        prevY = y;
      }
    }
  }
  
  /*-----------------------------------------------------**/
  // I mean.. yeah it does what it says. You figure it out
  boolean mouseInside(float x, float y, int w, int h){
    return x < mouseX && (x+w) > mouseX && (y + h) > mouseY && y < mouseY;
  }
   
  /*-----------------------------------------------------**/
  // Need this on more than one occasion, so
  // it gets its own function
  void getMinMax(int p){
    minY = min(table.getFloatColumn(p));
    maxY = max(table.getFloatColumn(p));
  }
  
  /*-----------------------------------------------------**/
  void mousePressed(){
    for(int q = 0; q < colCount; q++) {
      // check to see if mouse is inside any label
       if(mouseInside(lineX[q] - 10, Texty - 10, 20, 15)){
         // check to see if another label has been selected for switch
         if(SomethingSelected){
           int tmp = lineOrder[q];
           lineOrder[q] = lineOrder[selectedIndex];
           lineOrder[selectedIndex] = tmp;
           
           float temp2 = lineX[q];
           lineX[q] = lineX[selectedIndex];
           lineX[selectedIndex] = temp2;
             
           selectedIndex = -1;
           SomethingSelected = false;
           return;
         } 
         else {
           selectedIndex = q;
           SomethingSelected = true;
           return;
         }
       }
    }
  }
  
  /*-----------------------------------------------------**/
}