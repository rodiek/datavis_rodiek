class Splom extends Frame {
  
    ArrayList<Scatterplot> plots = new ArrayList<Scatterplot>( );
    int colCount;
    Table data;
    float border = 20;
    Scatterplot nw = null;
    
   Splom( ArrayList<Integer> useColumns ){
     colCount = useColumns.size();
     for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
           Scatterplot sp = new Scatterplot( useColumns.get(j), useColumns.get(i) );
           plots.add(sp);
       }
     }
   }
   
   void setPosition( int u0, int v0, int w, int h ){
     super.setPosition(u0,v0,w,h);

    int curPlot = 0;
    for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
          Scatterplot sp = plots.get(curPlot++);
           int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
           int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
           sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
           sp.drawLabels = false;
           sp.border = 3;
     }
    }
  }

   void draw() {
     for( Scatterplot s : plots ){
        s.draw(); 
     }
     if(nw!=null)
       nw.draw();
   }
   
  void mousePressed(){ 
    for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
          nw = sp;
          int colCount = data.getColumnCount();
          int x = (int)map( 0, 0, colCount, u0+border, u0+w-border );
          int y = (int)map( colCount-1, 0, colCount, v0+border, v0+h-border );
          int temp1 = (int)(w-2*border)/(colCount-1);
          int temp2 = (int)(h-2*border)/(colCount-1);
          nw.setPosition( x, y, temp1,temp2 );
       }
    }
  }
}