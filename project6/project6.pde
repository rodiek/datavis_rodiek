ArrayList<Frame> myFrames = new ArrayList<Frame>();
Frame bigFrame = null;
Table table;

void setup(){
  size(1200,800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> useColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        println( i + " - type float" );
        useColumns.add(i);
      }
      else{
        println( i + " - type string" );
      }
    }
    
    myFrames.add(new ParaCoor(useColumns));
    myFrames.add(new Splom(useColumns));
    myFrames.add(new Linechart( 0, 1 ));
    myFrames.add(new Scatterplot( 0, 1 ));
  }
}


void draw(){
  background( 255 );
  if( table == null ) 
    return;
    
  if( myFrames.size() == 4){
    for(Frame f: myFrames) {
      f.drawLabels = false;
    }
    myFrames.get(0).setPosition(0,0,300,400);
    myFrames.get(1).setPosition(300,0,300,400);
    myFrames.get(2).setPosition(0,400,300,400);
    myFrames.get(3).setPosition(300,400,300,400);
    for(Frame f: myFrames) {
     f.draw(); 
    }
    
  }
  if(bigFrame != null){
    bigFrame.setPosition(600,0,600,800);
    bigFrame.drawLabels = true;
    bigFrame.draw(); //<>//
  }
  else {
  text("<--- Select Diagram from Left", 900, 400);
  }
}


void mousePressed(){
  for(Frame f: myFrames) {
     if(f.mouseInside()) {
      bigFrame = f;
     }
       
  }
}
abstract class Frame {
  int u0,v0,w,h;
  int clickBuffer = 2;
  boolean drawLabels = true;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  abstract void draw();
  void mousePressed(){ }
  
  boolean mouseInside(){
     return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY; 
  }
}